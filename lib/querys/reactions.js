const request = require('../services/request');
const Enums = require('../services/enums');

const queryParts = {
    limit: "limit(0)",
    total_count: "summary(total_count)",
    as: "as(",
    type: "type(",
}

module.exports = function(post_id, access_token, type) {
    
    var url = typeof post_id != 'string'? new String(post_id) : post_id
    var params = {
        access_token,
        summary: true
    };

    if(type) {  

        var reactEntries = Object.entries(Enums.reactions);

        if(!reactEntries.some(reaction => { return reaction[1] == type })) {
            throw new Error('reactions(): invalid reaction type. Check Enums#reactions.');
        } 

        if(type == Enums.reactions.all) {

            var query = "";
            
            reactEntries = reactEntries.filter(react => { 
                var condition = react[1] != Enums.reactions.all && react[1] != Enums.reactions.none;
                return condition;
            });

            reactEntries.forEach(react => {
                query = query.concat('reactions.type(' + react[1] + ').limit(0).summary(total_count).as(' + react[0] + '),');
            });

            query = query.substring(0, query.length - 1);

            delete params.summary
            params.fields = query
        }
        else {
            url = url.concat('/reactions');
            params.type = type;
        }
    }
    else {
        url = url.concat('/reactions');
    }

    return request(url, params)
    .then(result => { 
        if(result) { 
            return result;
        } 
        else {
            throw new Error('reactions() result is undefined');
        }
    })
    .catch(err => { throw err; });
}