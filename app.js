const config = __graphConfig;
const authenticator = require('./lib/querys/auth');
const posts = require('./lib/querys/posts');
const reactions = require('./lib/querys/reactions');
const comments = require('./lib/querys/comments');
const Enums = require('./lib/services/enums');

var FacebookConnector = function(configuration)
{
	if (FacebookConnector.caller != FacebookConnector.getInstance)
	{
		throw new Error("This object cannot be instanciated");
    }
}

FacebookConnector.instance = null;
FacebookConnector.getInstance = function()
{
	if (this.instance === null)
	{
		this.instance = new FacebookConnector();
	}
	return this.instance;
};

FacebookConnector.prototype = {

    useUserToken: false, 

    setAppToken: function(callback) {

        var self = this;

        authenticator().then((token) => {
            self.access_token = token.access_token;
            if(callback) { callback() }
        })
        .catch(err => { callback(err) });
    },

    /**
     * Options
     * {
     *      amount,         //Number of posts to return, from most to least recent
     *                      //If @amount = -1 returns all posts from page.
     * 
     *      shares,         //Set to include total count of shares per post.
     * 
     *      reactions,      //Set to include total count of reactions per post.
     * 
     *      comments,       //Set to include total count of comments per post.
     *  }
     */
    posts: function(page_id, options, callback) {

        var postsArray = [];
        var accessToken = this.access_token;
        var postsOptions = options;
        options.limit = 100;
        var amount = 10; //defaults to 10
        var noMorePages = false;

        if(this.useUserToken)
        {
            accessToken = config.auth.userToken;
        }

        if(options && options.count) {
            amount = options.count;
        }

        var recursive = function(apiResponse) {

            if(apiResponse) {

                postsArray = postsArray.concat(apiResponse.data);

                if(apiResponse.paging.next) {
                    postsOptions.after = apiResponse.paging.cursors.after;
                }
                else {
                    noMorePages = true;
                }
            }

            if(noMorePages) {

                postsArray = postsArray.map(post => {

                    if(post.shares) { post.shares = post.shares.count }
                    if(post.reactions) { post.reactions = post.reactions.summary.total_count }
                    if(post.comments) { post.comments = post.comments.summary.total_count }

                    return post;
                });

                var result = {
                    data: postsArray,
                    total: postsArray.length,
                }
    
                callback(null, result);
            }
            else if(amount != -1 && amount <= 100) { //max limit imposed by GraphAPI

                postsOptions.limit = amount;

                posts(page_id, accessToken, postsOptions).then(lastResponse => {
                    
                    postsArray = postsArray.concat(lastResponse.data);
                    
                    noMorePages = true;

                    recursive();

                }).catch(err => callback(err) );
            } 
            else {
                amount = amount - 100; //remaining number of posts to return
                posts(page_id, accessToken, postsOptions).then(recursive).catch(err => callback(err) );
            }
        }

        new Promise(resolve => { recursive() });
    },

    comments: function(post_id, options, callback) {

        var commentsArray = [];
        var accessToken = this.access_token;
        var commentsOptions = options;
        options.limit = 100;
        var amount = 10; //defaults to 10
        var noMorePages = false;

        if(this.useUserToken)
        {
            accessToken = config.auth.userToken;
        }

        if(options && options.count) {
            amount = options.count;
        }

        var recursive = function(apiResponse) {

            if(apiResponse) {

                commentsArray = commentsArray.concat(apiResponse.data);

                if(apiResponse.paging.next) {
                    commentsOptions.after = apiResponse.paging.cursors.after;
                }
                else {
                    noMorePages = true;
                }
            }

            if(noMorePages) {

                commentsArray = commentsArray.map(comment => {
                    if(comment.reactions) { comment.reactions = comment.reactions.summary.total_count }
                    return comment;
                });

                var result = {
                    data: commentsArray,
                    total: commentsArray.length,
                }
    
                callback(null, result);
            }
            else if(amount != -1 && amount <= 100) { //max limit imposed by GraphAPI

                commentsOptions.limit = amount;

                comments(post_id, accessToken, commentsOptions).then(lastResponse => {
                    
                    commentsArray = commentsArray.concat(lastResponse.data);
                    
                    noMorePages = true;

                    recursive();

                }).catch(err => callback(err) );
            } 
            else {
                amount = amount - 100; //remaining number of posts to return
                comments(post_id, accessToken, commentsOptions).then(recursive).catch(err => callback(err) );
            }
        }

        new Promise(resolve => { recursive() });
    },

    reactions: function(post_id, type, callback) {

        var accessToken = this.access_token;

        if(this.useUserToken)
        {
            accessToken = config.auth.userToken;
        }

        reactions(post_id, accessToken, type).then(result => { 

            var ret = {}

            if(type && type == Enums.reactions.all) {
                
                delete result.id

                Object.keys(result).forEach(key => {
                    delete result[key].data;
                    result[key].total_count = result[key].summary.total_count;
                    delete result[key].summary
                });

                ret = result;
            }
            else {
                
                ret.total_count = result.summary.total_count;
            }

            callback(null, ret);

        }).catch(err => callback(err) );
    },
	
}

module.exports = FacebookConnector.getInstance();