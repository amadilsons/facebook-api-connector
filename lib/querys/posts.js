const request = require('../services/request');

/**
 * Options
 * {
 *      limit,         //Number of posts to return starting from the last one down, MAX 100
 * 
 *      shares,         //Set to include total number of shares per post
 * 
 *      after,          //Cursor for the next iteration
 * 
 *      before,         //Cursor for the previous iteration
 *
 * }
 */
module.exports = function(page_id, access_token, options) {
    
    var url = page_id + "/posts";
    var params = {
        access_token,
        fields: "created_time,message,story"
    };

    if(options) {
        if(options.limit) {
            params.limit = options.limit;
        }
        
        if(options.shares) {
            params.fields = params.fields.concat(',shares');
        }

        if(options.comments) {
            params.fields = params.fields.concat(',comments.limit(0).summary(total_count)');
        }

        if(options.reactions) {
            params.fields = params.fields.concat(',reactions.summary(total_count)');
        }

        if(options.after) {
            params.after = options.after;
        }
        else if(options.before) {
            params.before = options.before;
        }
    }

    return request(url, params)
    .then(result => { if(result) { return result }})
    .catch(err => { throw err; });
}