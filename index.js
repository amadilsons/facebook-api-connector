global.__config = require("./config/graphapi");

var enums = require('./lib/services/enums');
var connector = require('./app');


/*
connector.setAppToken((err) => { if(err) throw err; console.log('AppToken', connector.access_token) });

//For development purposes only
connector.useUserToken = true;

connector.getAllPosts("MichaelJacksonsHealthCondition", { shares: true }, (err, data) => { if(err) { console.log('0', err) } else console.log(data) });

connector.getPosts("vodafonePT", { amount: 202, shares: true, comments: true, reactions: true }, (err, data) => { if(err) { console.log('1', err) } else console.log(data) });

connector.getAllComments("122118287806643_1835483999803388", { reactions: true }, (err, data) => { if(err) { console.log('3', err) } else console.log(data) });

connector.getComments("122118287806643_1835483999803388", { amount: 30, reactions: true }, (err, data) => { if(err) { console.log('2', err) } else console.log(data) });

connector.getReactions("1835483999803388_1836238329727955", Enums.reactions.all, (err, data) => { if(err) { console.log('2', err) } else console.log(data) });
*/

module.exports = {
    enums,
    connector
}