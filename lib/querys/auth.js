const request = require('../services/request');
const config = __config;

module.exports = function() {
    
    var baseURL = __config.baseURL + __config._v;
    var url = "/oauth/access_token"
    var params = __config.auth;
    params.grant_type = "client_credentials";

    return request(url, params)
    .then(result => { if(result) { return result } else {
        throw new Error('auth() result is undefined');
    }})
    .catch(err => { throw err; });
}