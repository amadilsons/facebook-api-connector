var REACTION_TYPES = {
    none: "NONE",
    all: "ALL",
    like: "LIKE",
    love: "LOVE",
    wow: "WOW",
    haha: "HAHA",
    sad: "SAD",
    angry: "ANGRY",
    thankful: "THANKFUL"
}

module.exports = {

    reactions: REACTION_TYPES,
    
}