const axios = require('axios');

module.exports = function(url, params) {
    
    var baseURL = __config.baseURL + __config._v;

    return axios.get(url, { baseURL, params })
    .then(response => { return response.data; })
    .catch(err => { 

        if(err.response)
        {
            err = err.response.data.error;
        }
        else if(err.request)
        {
            err = err.request;
        } 

        throw !err.message? err : new Error(err.message);
    });
}